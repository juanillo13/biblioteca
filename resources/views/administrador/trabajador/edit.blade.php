@extends('administrador.main')

@section('title','Editar trabajador: '.$trabajador->nombre.' '.$trabajador->apellido_paterno)

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar trabajador</div>
					<div class="panel-body">
						<div class="col-md-12">
                        {!! Form::open(['route' => ['trabajadores.update',$trabajador], 'method' => 'PUT']) !!}
                                    <div class="form-group">
                                        {!! Form::label('rut','RUT')!!}
                                        {!! Form::text('rut',$trabajador->rut,['class' => ['form-control','rut'],'required'])!!}
                                    </div>
                                    
                                    <div class="form-group">
                                        {!! Form::label('nombre','Nombre')!!}
                                        {!! Form::text('nombre',$trabajador->nombre,['class' => 'form-control', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_paterno','Apellido Paterno')!!}
                                        {!! Form::text('apellido_paterno',$trabajador->apellido_paterno,['class' => 'form-control'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_materno','Apellido Materno')!!}
                                        {!! Form::text('apellido_materno',$trabajador->apellido_materno,['class' => 'form-control'])!!}
                                    </div>
                                    <div class="form-group">

                                    <div class="form-group">
                                        {!! Form::label('anio_contrato','Año inico de contrato')!!}
                                        {!! Form::text('anio_contrato',$trabajador->anio_contrato,['class' => 'form-control'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

                        </div>
					</div>
				</div>
			</div><!-- /.panel-->

    @endsection


@section('js')

        <script type="text/javascript">
            
        $(".rut")
        .rut({formatOn: 'keyup', validateOn: 'keyup'})
        .on('rutInvalido', function(){ 
            $(this).parents(".control-group").addClass("error")
        })
        .on('rutValido', function(){ 
        $(this).parents(".control-group").removeClass("error")
        });
        </script>

@endsection
