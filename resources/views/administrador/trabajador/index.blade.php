

@extends('administrador.main')

@section('title','Mantenedor de Trabajadores')

@section('content')


    {!! Form::open(['route' => 'trabajadores.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar por nombre..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('trabajadores.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Trabajador</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Trabajadores</div>
		<div class="panel-body">
			<div class="col-md-12 table-responsive">
                <table class="table table-bordered">
                <thead>
                        <th>ID</th>
                        <th>RUT</th>
                        <th >Nombres</th>
                        <th>Apellidos</th>
                        <th >Año inicio de contrato</th>
                        <th class="col-md-3">Emails</th>
                        <th class="col-md-3">Teléfonos</th>
                        <th class="col-md-3">direcciones</th>
                        <th class="col-md-2">Acción</th>
                </thead>
                <tbody>
                        @foreach($trabajadores as $trabajador)
                            <tr>
                                <td>{{ $trabajador->id }}</td>
                                <td>{{ $trabajador->rut}}</td>
                                <td>{{ $trabajador->nombre }}</td>
                                <td>{{ $trabajador->apellido_paterno.' '.$trabajador->apellido_materno}}</td>
                                
                                <td>{{ $trabajador->anio_contrato }}</td>

                                <td>                                 
                                @foreach($trabajador->correos as $correo)
                                    <li>{{ $correo->correo}}</li>
                                    @endforeach
                                </td>
                                
                                <td>                                 
                                @foreach($trabajador->telefonos as $telefono)
                                    <li>{{ $telefono->telefono}}</li>
                                    @endforeach
                                </td>
                                <td>                                 
                                @foreach($trabajador->direcciones as $direccion)
                                    <li>{{ $direccion->direccion}}</li>
                                    @endforeach
                                </td>
   
                                <td> 
                                    <a href="{{route('trabajadores.edit',$trabajador->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('trabajadores.destroy',$trabajador->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $trabajadores->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection

