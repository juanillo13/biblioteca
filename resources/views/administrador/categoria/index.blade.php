@extends('administrador.main')

@section('title','Mantenedor de Categorías')

@section('content')


    {!! Form::open(['route' => 'categorias.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar Categoria..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('categorias.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Categoría</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Categorias</div>
		<div class="panel-body">
			<div class="col-md-12 table-responsive">
                <table class="table table-bordered">
                <thead>
                        <th class="col-md-2">ID</th>
                        <th>Nombre</th>
                        <th class="col-md-2">Acción</th>
                </thead>
                <tbody>
                        @foreach($categorias as $categoria)
                            <tr>
                                <td>{{ $categoria->id }}</td>
                                <td>{{ $categoria->categoria }}</td>
   
                                <td> 
                                    <a href="{{route('categorias.edit',$categoria->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('categorias.destroy',$categoria->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $categorias->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection

