@extends('administrador.main')

@section('title','Editar Categoría: '.$categoria->categoria)

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar Categoría</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => ['categorias.update',$categoria], 'method' => 'PUT']) !!}
                                    <div class="form-group">
                                        {!! Form::label('categoria','Categoría')!!}
                                        {!! Form::text('categoria',$categoria->categoria,['class' => 'form-control', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection