@extends('administrador.main')

@section('title','Crear una nueva categoría')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nueva categoría</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'categorias.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('categoria','categoría')!!}
                                        {!! Form::text('categoria',Null,['class' => 'form-control', 'placeholder' => 'ejemplo: Ciencia ficción', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection
