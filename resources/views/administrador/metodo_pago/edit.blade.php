@extends('administrador.main')

@section('title','Editar método de pago: '.$pago->metodo_pago)

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar Categoría</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => ['pagos.update',$pago], 'method' => 'PUT']) !!}
                                    <div class="form-group">
                                        {!! Form::label('metodo_pago','Método de pago')!!}
                                        {!! Form::text('metodo_pago',$pago->metodo_pago,['class' => 'form-control', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection