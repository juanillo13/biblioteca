@extends('administrador.main')

@section('title','Mantenedor de métodos de pago')

@section('content')


    {!! Form::open(['route' => 'pagos.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar métodos..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('pagos.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Método de pago</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Métodos de pago</div>
		<div class="panel-body">
			<div class="col-md-12 table-responsive">
                <table class="table table-bordered">
                <thead>
                        <th class="col-md-2">ID</th>
                        <th>Método de pago</th>
                        <th class="col-md-2">Acción</th>
                </thead>
                <tbody>
                        @foreach($pagos as $pago)
                            <tr>
                                <td>{{ $pago->id }}</td>
                                <td>{{ $pago->metodo_pago }}</td>
   
                                <td> 
                                    <a href="{{route('pagos.edit',$pago->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('pagos.destroy',$pago->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $pagos->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection
