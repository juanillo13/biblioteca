@extends('administrador.main')

@section('title','Crear un nuevo método de pago')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nuevo método de pago</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'pagos.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('metodo_pago','Método de pago')!!}
                                        {!! Form::text('metodo_pago',Null,['class' => 'form-control', 'placeholder' => 'ejemplo: Tarjeta de crédito', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection