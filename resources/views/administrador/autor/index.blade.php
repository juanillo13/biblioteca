@extends('administrador.main')

@section('title','Mantenedor de Autores')

@section('content')


    {!! Form::open(['route' => 'autores.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar por nombre..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('autores.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Autor</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Autores</div>
		<div class="panel-body">
			<div class="col-md-12">
                <table class="table table-bordered">
                <thead>
                        <th class="col-md-1">ID</th>
                        <th class="col-md-3">Nombre</th>
                        <th class="col-md-2">Apellido paterno</th>
                        <th class="col-md-2">Apellido materno</th>
                        <th class="col-md-2">Acción</th>
                </thead>
                <tbody>
                        @foreach($autores as $autor)
                            <tr>
                                <td>{{ $autor->id }}</td>
                                <td>{{ $autor->nombre }}</td>
                                <td>{{ $autor->apellido_paterno }}</td>
                                <td>{{ $autor->apellido_materno }}</td>
   
                                <td> 
                                    <a href="{{route('autores.edit',$autor->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('autores.destroy',$autor->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $autores->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection

