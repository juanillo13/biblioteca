@extends('administrador.main')

@section('title','Crear un nuevo autor')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nuevo autor</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'autores.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('nombre','Nombre')!!}
                                        {!! Form::text('nombre',Null,['class' => 'form-control', 'placeholder' => 'Ambos nombres', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_paterno','Apellido Paterno')!!}
                                        {!! Form::text('apellido_paterno',Null,['class' => 'form-control', 'placeholder' => ''])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_materno','Apellido Materno')!!}
                                        {!! Form::text('apellido_materno',Null,['class' => 'form-control', 'placeholder' => ''])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection