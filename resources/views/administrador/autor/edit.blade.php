@extends('administrador.main')

@section('title','Editar Autor: '.$autor->nombre)

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar autor</div>
					<div class="panel-body">
						<div class="col-md-12">
                        {!! Form::open(['route' => ['autores.update',$autor], 'method' => 'PUT']) !!}
                                    <div class="form-group">
                                        {!! Form::label('nombre','Nombre')!!}
                                        {!! Form::text('nombre',$autor->nombre,['class' => 'form-control', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_paterno','Apellido Paterno')!!}
                                        {!! Form::text('apellido_paterno',$autor->apellido_paterno,['class' => 'form-control'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_materno','Apellido Materno')!!}
                                        {!! Form::text('apellido_materno',$autor->apellido_materno,['class' => 'form-control'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

                        </div>
					</div>
				</div>
			</div><!-- /.panel-->

    @endsection