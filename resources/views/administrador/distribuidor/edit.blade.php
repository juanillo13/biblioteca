@extends('administrador.main')

@section('title','Editar Distribuidor: '.$distribuidor->nombre_empresa)

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar distribuidor</div>
					<div class="panel-body">
						<div class="col-md-12">
                        {!! Form::open(['route' => ['distribuidores.update',$distribuidor], 'method' => 'PUT']) !!}
                        <div class="form-group"> 
                                {!! Form::label('rut','RUT empresa')!!}
                                {!! Form::text('rut',$distribuidor->rut,['class' => ['form-control','rut'], 'required'])!!}
                        </div>
                        <div class="form-group">
                                {!! Form::label('nombre_empresa','Nombre empresa')!!}
                                {!! Form::text('nombre_empresa',$distribuidor->nombre_empresa,['class' => 'form-control','required'])!!}
                        </div>

                        <div class="form-group">
                                {!! Form::label('direccion','Dirección')!!}
                                {!! Form::text('direccion',$distribuidor->direccion,['class' => 'form-control','required'])!!}
                        </div>

                        <div class="form-group">
                                {!! Form::label('telefono','Teléfono')!!}
                                {!! Form::text('telefono',$distribuidor->telefono,['class' => 'form-control','required'])!!}
                        </div>
                        <div class="form-group">
                                {!! Form::label('anio_venta_libro','Año de inicio venta de libros')!!}
                                {!! Form::text('anio_venta_libro',$distribuidor->anio_venta_libro,['class' => 'form-control','required'])!!}
                        </div>
                        <div class="form-group">         
                                {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}
                        </div>		

                        {!! Form::close() !!}

                        </div>
					</div>
				</div>
			</div><!-- /.panel-->

    @endsection


@section('js')

        <script type="text/javascript">
            
        $(".rut")
        .rut({formatOn: 'keyup', validateOn: 'keyup'})
        .on('rutInvalido', function(){ 
            $(this).parents(".control-group").addClass("error")
        })
        .on('rutValido', function(){ 
        $(this).parents(".control-group").removeClass("error")
        });
        </script>

@endsection
