@extends('administrador.main')

@section('title','Mantenedor de Distribuidores')

@section('content')


    {!! Form::open(['route' => 'distribuidores.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar por nombre..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('distribuidores.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Distribuidor</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Distribuidores</div>
		<div class="panel-body">
			<div class="col-md-12 table-responsive">
                <table class="table table-bordered">
                <thead>
                        <th>ID</th>
                        <th>RUT empresa</th>
                        <th >Nombres empresa</th>
                        <th>Dirección</th>
                        <th >Teléfono</th>
                        <th >año de inicio de ventas</th>
                        <th >Acción</th>

                </thead>
                <tbody>
                        @foreach($distribuidores as $distribuidor)
                            <tr>
                                <td>{{ $distribuidor->id }}</td>
                                <td>{{ $distribuidor->rut}}</td>
                                <td>{{ $distribuidor->nombre_empresa }}</td>
                                <td>{{ $distribuidor->direccion }}</td> 
                                <td>{{ $distribuidor->telefono }}</td>                          
                                <td>{{ $distribuidor->anio_venta_libro }}</td>  
                                
                                <td> 
                                    <a href="{{route('distribuidores.edit',$distribuidor->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('distribuidores.destroy',$distribuidor->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $distribuidores->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection
