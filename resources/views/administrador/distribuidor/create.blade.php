@extends('administrador.main')

@section('title','Crear un nuevo distribuidor')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nuevo distribuidor</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'distribuidores.store', 'method' => 'POST']) !!}
                                <div class="form-group"> 
                                        {!! Form::label('rut','RUT empresa')!!}
                                        {!! Form::text('rut',Null,['class' => ['form-control','rut'], 'placeholder' => '11.111.111-1', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('nombre_empresa','Nombre empresa')!!}
                                        {!! Form::text('nombre_empresa',Null,['class' => 'form-control', 'placeholder' => '', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('direccion','Dirección')!!}
                                        {!! Form::text('direccion',Null,['class' => 'form-control', 'placeholder' => ' Calle #número','required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('telefono','Teléfono')!!}
                                        {!! Form::text('telefono',Null,['class' => 'form-control', 'placeholder' => '+569 99999999','required'])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('anio_venta_libro','Año de inicio venta de libros')!!}
                                        {!! Form::text('anio_venta_libro',Null,['class' => 'form-control', 'placeholder' => '','required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    
@endsection


@section('js')

<script type="text/javascript">
	
$(".rut")
  .rut({formatOn: 'keyup', validateOn: 'keyup'})
  .on('rutInvalido', function(){ 
    $(this).parents(".control-group").addClass("error")
  })
  .on('rutValido', function(){ 
$(this).parents(".control-group").removeClass("error")
  });
</script>

@endsection