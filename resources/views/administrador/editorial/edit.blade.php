@extends('administrador.main')

@section('title','Editar Estitorial: '.$editorial->nombre_editorial)

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar Editorial</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => ['editoriales.update',$editorial], 'method' => 'PUT']) !!}
                                    <div class="form-group">
                                        {!! Form::label('editorial','Nombre Editorial')!!}
                                        {!! Form::text('nombre_editorial',$editorial->nombre_editorial,['class' => 'form-control', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection