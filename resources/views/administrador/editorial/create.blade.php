@extends('administrador.main')

@section('title','Crear una nueva editorial')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nueva editorial</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'editoriales.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('editorial','Editorial')!!}
                                        {!! Form::text('nombre_editorial',Null,['class' => 'form-control', 'placeholder' => 'ejemplo: Panini', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection
