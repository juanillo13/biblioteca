@extends('administrador.main')

@section('title','Mantenedor de Editoriales')

@section('content')


    {!! Form::open(['route' => 'editoriales.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar Editorial..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('editoriales.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Editorial</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Editoriales</div>
		<div class="panel-body">
			<div class="col-md-12">
                <table class="table table-bordered">
                <thead>
                        <th class="col-md-2">ID</th>
                        <th>Nombre</th>
                        <th class="col-md-2">Acción</th>
                </thead>
                <tbody>
                        @foreach($editoriales as $editorial)
                            <tr>
                                <td>{{ $editorial->id }}</td>
                                <td>{{ $editorial->nombre_editorial }}</td>
   
                                <td> 
                                    <a href="{{route('editoriales.edit',$editorial->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('editoriales.destroy',$editorial->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $editoriales->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection

