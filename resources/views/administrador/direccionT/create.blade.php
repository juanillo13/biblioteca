@extends('administrador.main')

@section('title','Crear una nueva dirección')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nueva Dirección</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'direccionest.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('direccion','Dirección')!!}
                                        {!! Form::text('direccion',Null,['class' => 'form-control', 'placeholder' => 'ejemplo: Población Calle #Número', 'required'])!!}
                                    </div>
 
                                    <div class="form-group">
			                            {!! Form::label('trabajador_id', 'Trabajadores') !!}
			                            {!! Form::select('trabajador_id', $trabajadores,NULL,['class' => 'form-control','placeholder' => 'Seleccione una opción...','required'])!!}
		                            </div>

                                    
                                    <div class="form-group">                     
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}
                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection