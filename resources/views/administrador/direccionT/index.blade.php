@extends('administrador.main')

@section('title','Mantenedor de Direcciones para trabajadores')

@section('content')


    {!! Form::open(['route' => 'direccionest.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar Dirección..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('direccionest.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Dirección</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Direcciones</div>
		<div class="panel-body">
			<div class="col-md-12">
                <table class="table table-bordered">
                <thead>
                        <th class="col-md-2">ID</th>
                        <th>Dirección</th>
                        <th>Trabajador</th>
                        <th class="col-md-2">Acción</th>
                        
                </thead>
                <tbody>
                        @foreach($direcciones as $direccion)
                            <tr>
                                <td>{{ $direccion->id }}</td>
                                <td>{{ $direccion->direccion }}</td>
                                <td>{{ $direccion->trabajador->nombre.' '.$direccion->trabajador->apellido_paterno }}</td>

   
                                <td> 
                                    <a href="{{route('direccionest.edit',$direccion->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('direccionest.destroy',$direccion->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $direcciones->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection