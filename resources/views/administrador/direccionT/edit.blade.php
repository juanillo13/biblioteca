@extends('administrador.main')

@section('title','Editar Dirección: '.$direccion->direccion )

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar Dirección</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => ['direccionest.update',$direccion], 'method' => 'PUT']) !!}
                                <div class="form-group">
                                        {!! Form::label('direccion','Dirección')!!}
                                        {!! Form::text('direccion',$direccion->direccion,['class' => 'form-control','required'])!!}
                                    </div>
 
                                    <div class="form-group">
			                            {!! Form::label('trabajador_id', 'Trabajadores') !!}
			                            {!! Form::select('trabajador_id', $trabajadores,$direccion->trabajador->id,['class' => 'form-control','required'])!!}
		                            </div>


                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection