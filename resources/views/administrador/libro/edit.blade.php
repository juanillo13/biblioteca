@extends('administrador.main')

@section('title','Editar Libro: '.$libro->titulo )

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar Libro</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => ['libros.update',$libro], 'method' => 'PUT']) !!}
                                <div class="form-group">
                                        {!! Form::label('isbn','Isbn')!!}
                                        {!! Form::text('isbn',$libro->isbn,['class' => 'form-control', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('titulo','Título')!!}
                                        {!! Form::text('titulo',$libro->titulo,['class' => 'form-control','required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('n_paginas','Número de Páginas')!!}
                                        {!! Form::number('n_paginas',$libro->n_paginas,['class' => 'form-control','min' =>'0','required'])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('p_refrencia','Precio de Referencia')!!}
                                        {!! Form::number('p_refrencia',$libro->p_refrencia,['class' => 'form-control','min' =>'0','required'])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('año_publicacion','Año de Publicación')!!}
                                        {!! Form::number('año_publicacion',$libro->año_publicacion,['class' => 'form-control','min' =>'0','required'])!!}
                                    </div>
                                    <div class="form-group">
			                            {!! Form::label('estado_id', 'Estado') !!}
			                            {!! Form::select('estado_id', $estados,$libro->estado->id,['class' => 'form-control','required'])!!}
		                            </div>

                                    <div class="form-group">
			                            {!! Form::label('editoriales', 'Editoriales') !!}
			                            {!! Form::select('editoriales[]', $editoriales,$mis_editoriales,['class' => 'form-control chosen-select','multiple','required'])!!}
		                            </div>
                                    <div class="form-group">
			                            {!! Form::label('categorias', 'Categorías') !!}
			                            {!! Form::select('categorias[]', $categorias,$mis_categorias,['class' => 'form-control chosen-select','multiple','required'])!!}
		                            </div>
                                    <div class="form-group">
			                            {!! Form::label('idiomas', 'Idiomas') !!}
			                            {!! Form::select('idiomas[]', $idiomas,$mis_idiomas,['class' => 'form-control chosen-select','multiple','required'])!!}
		                            </div>
                                    <div class="form-group">
			                            {!! Form::label('autores', 'Autores') !!}
			                            {!! Form::select('autores[]', $autores,$mis_autores,['class' => 'form-control chosen-select','multiple','required'])!!}
		                            </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection


@section('js')

<script type="text/javascript">
	
	$(".chosen-select").chosen({

		placeholder_text_multiple: 'seleccione múltiples opciones',
		no_results_text: "No se encontraron coincidencias!!"
	});
</script>

@endsection