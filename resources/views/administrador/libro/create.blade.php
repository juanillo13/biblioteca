@extends('administrador.main')

@section('title','Crear un nuevo Libro')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nuevo libro</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'libros.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('isbn','Isbn')!!}
                                        {!! Form::text('isbn',Null,['class' => 'form-control', 'placeholder' => 'mínimo 10 dígitos', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('titulo','Título')!!}
                                        {!! Form::text('titulo',Null,['class' => 'form-control', 'placeholder' => 'Título completo del libro','required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('n_paginas','Número de Páginas')!!}
                                        {!! Form::number('n_paginas',Null,['class' => 'form-control', 'placeholder' => '','min' =>'0','required'])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('p_refrencia','Precio de Referencia')!!}
                                        {!! Form::number('p_refrencia',Null,['class' => 'form-control', 'placeholder' => '','min' =>'0','required'])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('año_publicacion','Año de Publicación')!!}
                                        {!! Form::number('año_publicacion',Null,['class' => 'form-control', 'placeholder' => '','min' =>'0','required'])!!}
                                    </div>
                                    <div class="form-group">
			                            {!! Form::label('estado_id', 'Estados') !!}
			                            {!! Form::select('estado_id', $estados,NULL,['class' => 'form-control','placeholder' => 'Seleccione una opción...','required'])!!}
		                            </div>

                                    <div class="form-group">
			                            {!! Form::label('editoriales', 'Editoriales') !!}
			                            {!! Form::select('editoriales[]', $editoriales,NULL,['class' => 'form-control chosen-select','multiple','required'])!!}
		                            </div>
                                    <div class="form-group">
			                            {!! Form::label('categorias', 'Categorías') !!}
			                            {!! Form::select('categorias[]', $categorias,NULL,['class' => 'form-control chosen-select','multiple','required'])!!}
		                            </div>
                                    <div class="form-group">
			                            {!! Form::label('idiomas', 'Idiomas') !!}
			                            {!! Form::select('idiomas[]', $idiomas,NULL,['class' => 'form-control chosen-select','multiple','required'])!!}
		                            </div>
                                    <div class="form-group">
			                            {!! Form::label('autores', 'Autores') !!}
			                            {!! Form::select('autores[]', $autores,NULL,['class' => 'form-control chosen-select','multiple','required'])!!}
		                            </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection

@section('js')

<script type="text/javascript">
	
	$(".chosen-select").chosen({

		placeholder_text_multiple: 'seleccione múltiples opciones',
		no_results_text: "No se encontraron coincidencias!!"
	});
</script>

@endsection