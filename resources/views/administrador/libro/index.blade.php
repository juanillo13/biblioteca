@extends('administrador.main')

@section('title','Mantenedor de Libros')

@section('content')


    {!! Form::open(['route' => 'libros.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar por titulo..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('libros.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Libro</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Libros</div>
		<div class="panel-body">
			<div class="col-md-12 table-responsive">
                <table class="table table-bordered">
                <thead>
                        <th >ID</th>
                        <th  >ISBN</th>
                        <th class="col-sm-3">Título</th>

                        <th>precio</th>

                        <th class="col-sm-2">idiomas</th>
                        <th>Editoriales</th>
                        <th class="col-sm-2">Autores</th>
                        <th class="col-sm-2">Categorías</th>
                        <th>Estado</th>

                      <th class="col-sm-3">Acción</th>
                </thead>
                <tbody>
                       @foreach($libros as $libro)
                            <tr>
                                <td>{{ $libro->id }}</td>
                                <td>{{ $libro->isbn }}</td>
                                <td>{{ $libro->titulo }}</td>
 
                                <td>{{ $libro->p_refrencia }}</td>

                                <td>
                                    
                                    @foreach($libro->idiomas as $idioma)
                                    <li>{{ $idioma->idioma}}</li>
                                    @endforeach
                                </td>

                                 <td> 
                                    @foreach($libro->editoriales as $editorial)
                                    <li>{{ $editorial->nombre_editorial}}</li>
                                    @endforeach
                                </td>
                                 <td>
                                    @foreach($libro->autores as $autor)
                                    <li>{{ $autor->nombre}}</li>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($libro->categorias as $categoria)
                                    <li>{{ $categoria->categoria}}</li>
                                    @endforeach
                                </td>

                                <td>{{ $libro->estado->estado }}</td>

                                <td> 
                                    <a href="{{route('libros.edit',$libro->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('libros.destroy',$libro->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $libros->render() }}<!--es para a paginación-->
                
            </div>
        </div>
    </div>
@endsection