@extends('administrador.main')

@section('title','Mantenedor de Usuarios')

@section('content')

<div class="container">
	<a href="{{ route('usuarios.create')}}" class="btn btn-info"><i class="fa fa-user-plus"></i> usuario</a>
</div>	
</br>
<div class="panel panel-default">
	<div class="panel-heading">Usuarios</div>
		<div class="panel-body">
			<div class="col-md-12">
                <table class="table table-bordered">
                <thead>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Tipo</th>
                        <th>Acción</th>
                </thead>
                <tbody>
                        @foreach($usuarios as $usuario)
                            <tr>
                                <td>{{ $usuario->id }}</td>
                                <td>{{ $usuario->name }}</td>
                                <td>{{ $usuario->email }}</td>
                            
                                <td>
                                    <h4><span class="label label-primary">{{ $usuario->tipo }}</span></h4>
                                </td>
                             
                                <td> 
                                    <a href="{{route('usuarios.edit',$usuario->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('usuarios.destroy',$usuario->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $usuarios->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection

<!---->