@extends('administrador.main')

@section('title','Editar usuario: '.$usuario->name)

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">editar Usuario</div>
					<div class="panel-body">
						<div class="col-md-12">
                            
                                {!! Form::open(['route' => ['usuarios.update',$usuario], 'method' => 'PUT']) !!}
                                    <div class="form-group">
                                        {!! Form::label('name','Nombre')!!}
                                        {!! Form::text('name',$usuario->name,['class' => 'form-control', 'required'])!!}

                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('email','Correo Electrónico')!!}
                                        {!! Form::email('email',$usuario->email,['class' => 'form-control','required'])!!}

                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('tipo','Tipo de Usuario')!!}
                                        {!! Form::select('tipo',['cliente' => 'Cliente','trabajador' => 'Trabajador del local','administrador' => 'Administrador de Sitio'],$usuario->tipo,['class'=>'form-control']) !!}

                                    </div>

                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection