@extends('administrador.main')

@section('title','Crear un nuevo usuario')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nuevo Usuario</div>
					<div class="panel-body">
						<div class="col-md-12">
                            
                                {!! Form::open(['route' => 'usuarios.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('name','Nombre')!!}
                                        {!! Form::text('name',Null,['class' => 'form-control', 'placeholder' => 'Nombre completo', 'required'])!!}

                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('email','Correo Electrónico')!!}
                                        {!! Form::email('email',Null,['class' => 'form-control', 'placeholder' => 'ejemplo@ejemplo.cl', 'required'])!!}

                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('password','Password')!!}
                                        {!! Form::password('password',['class' => 'form-control', 'placeholder' => '*****', 'required'])!!}

                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('tipo','Tipo de Usuario')!!}
                                        {!! Form::select('tipo',['cliente' => 'Cliente','trabajador' => 'Trabajador del local','administrador' => 'Administrador de Sitio'],Null,['class'=>'form-control']) !!}

                                    </div>

                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection




<!--

                            <form role="form">
								<div class="form-group">
									<label>Text Input</label>
									<input class="form-control" placeholder="Placeholder">
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" class="form-control">
								</div>


									<button type="submit" class="btn btn-primary">Submit Button</button>
									<button type="reset" class="btn btn-default">Reset Button</button>
							   
                            </form>

-->