@extends('administrador.main')

@section('title','Editar Correo electrónico: '.$correo->correo )

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar Correo electrónico</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => ['correosc.update',$correo], 'method' => 'PUT']) !!}
                                <div class="form-group">
                                        {!! Form::label('correo','Correo electrónico')!!}
                                        {!! Form::email('correo',$correo->correo,['class' => 'form-control', 'required'])!!}
                                    </div>
 
                                    <div class="form-group">
			                            {!! Form::label('cliente_id', 'Clientes') !!}
			                            {!! Form::select('cliente_id', $clientes,$correo->cliente->id,['class' => 'form-control','required'])!!}
		                            </div>

                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection