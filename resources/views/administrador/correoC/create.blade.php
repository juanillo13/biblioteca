@extends('administrador.main')

@section('title','Crear un nuevo Correo electrónico')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nuevo Correo electrónico</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'correosc.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('correo','Correo electrónico')!!}
                                        {!! Form::email('correo',Null,['class' => 'form-control', 'placeholder' => 'ejemplo:example@example.com', 'required'])!!}
                                    </div>
 
                                    <div class="form-group">
			                            {!! Form::label('cliente_id', 'Clientes') !!}
			                            {!! Form::select('cliente_id', $clientes,NULL,['class' => 'form-control','placeholder' => 'Seleccione una opción...','required'])!!}
		                            </div>

                                    
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection