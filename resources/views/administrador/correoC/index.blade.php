@extends('administrador.main')

@section('title','Mantenedor de correos para clientes')

@section('content')


    {!! Form::open(['route' => 'correosc.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar Email..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('correosc.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Correo Electrónico</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Correos Electrónicos</div>
		<div class="panel-body">
			<div class="col-md-12">
                <table class="table table-bordered">
                <thead>
                        <th class="col-md-2">ID</th>
                        <th>Correo Electrónico</th>
                        <th>Trabajador</th>
                        <th class="col-md-2">Acción</th>
                        
                </thead>
                <tbody>
                        @foreach($correos as $correo)
                            <tr>
                                <td>{{ $correo->id }}</td>
                                <td>{{ $correo->correo }}</td>
                                <td>{{ $correo->cliente->nombre.' '.$correo->cliente->apellido_paterno }}</td>

   
                                <td> 
                                    <a href="{{route('correosc.edit',$correo->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('correosc.destroy',$correo->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $correos->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection