@extends('administrador.main')

@section('title','Crear un nuevo teléfono')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nuevo teléfono</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'telefonost.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('telefono','Teléfono')!!}
                                        {!! Form::text('telefono',Null,['class' => 'form-control', 'placeholder' => 'ejemplo:+569 99999999', 'required'])!!}
                                    </div>
 
                                    <div class="form-group">
			                            {!! Form::label('trabajador_id', 'Trabajadores') !!}
			                            {!! Form::select('trabajador_id', $trabajadores,NULL,['class' => 'form-control','placeholder' => 'Seleccione una opción...','required'])!!}
		                            </div>

                                    
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection