@extends('administrador.main')

@section('title','Mantenedor de teléfonos para trabajadores')

@section('content')


    {!! Form::open(['route' => 'telefonost.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar Teléfono..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('telefonost.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Teléfono</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Teléfonos</div>
		<div class="panel-body">
			<div class="col-md-12">
                <table class="table table-bordered">
                <thead>
                        <th class="col-md-2">ID</th>
                        <th>Teléfono</th>
                        <th>Trabajador</th>
                        <th class="col-md-2">Acción</th>
                        
                </thead>
                <tbody>
                        @foreach($telefonos as $telefono)
                            <tr>
                                <td>{{ $telefono->id }}</td>
                                <td>{{ $telefono->telefono }}</td>
                                <td>{{ $telefono->trabajador->nombre.' '.$telefono->trabajador->apellido_paterno }}</td>

   
                                <td> 
                                    <a href="{{route('telefonost.edit',$telefono->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('telefonost.destroy',$telefono->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $telefonos->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection