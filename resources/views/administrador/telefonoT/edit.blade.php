@extends('administrador.main')

@section('title','Editar teléfono: '.$telefono->telefono )

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar Teléfono</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => ['telefonost.update',$telefono], 'method' => 'PUT']) !!}
                                <div class="form-group">
                                        {!! Form::label('telefono','Teléfono')!!}
                                        {!! Form::text('telefono',$telefono->telefono,['class' => 'form-control','required'])!!}
                                    </div>
 
                                    <div class="form-group">
			                            {!! Form::label('trabajador_id', 'Trabajadores') !!}
			                            {!! Form::select('trabajador_id', $trabajadores,$telefono->trabajador->id,['class' => 'form-control','required'])!!}
		                            </div>

                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection