
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Administración</title>
    <link rel="stylesheet" type="text/css" href="{{asset('styles/bootstrap.min.css')}}">
    <link href="{{asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('styles/datepicker3.css')}}" rel="stylesheet">
	<link href="{{asset('styles/adminstyles.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('chosen/chosen.css')}}">

	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="#"><span>Administración </span>Biblioteca</a>
				
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">

			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{auth()->user()->name}}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
				<div class="profile-usertitle-status"><span class="indicator label-danger"></span>{{auth()->user()->tipo}}</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li class=""><a href="{{ route('administrador')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li><a href="{{ route('usuarios.index')}}"><em class="fa fa-user">&nbsp;</em> Usuarios</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-th-list">&nbsp;</em> Libros <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="{{ route('estados.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Estados
					</a></li>
					<li><a class="" href="{{ route('editoriales.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Editoriales
					</a></li>
					<li><a class="" href="{{ route('categorias.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Categorias
					</a></li>
					<li><a class="" href="{{ route('idiomas.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Idiomas
					</a></li>
					<li><a class="" href="{{ route('autores.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Autores
					</a></li>
					<li><a class="" href="{{ route('libros.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Libros
					</a></li>
				</ul>
			</li>
			
			<li class="parent "><a data-toggle="collapse" href="#sub-item-2">
				<em class="fa fa-th-list">&nbsp;</em> Trabajadores <span data-toggle="collapse" href="#sub-item-2" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-2">
					<li><a class="" href="{{ route('trabajadores.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Trabajadores
					</a></li>
					<li><a class="" href="{{ route('correost.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Correos Electrónicos
					</a></li>
					<li><a class="" href="{{ route('telefonost.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Teléfonos
					</a></li>
					<li><a class="" href="{{ route('direccionest.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Direcciones
					</a></li>
				</ul>
			</li>

			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-th-list">&nbsp;</em> Clientes <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="{{ route('clientes.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Clientes
					</a></li>
					<li><a class="" href="{{ route('correosc.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Correos Electrónicos
					</a></li>
					<li><a class="" href="{{ route('telefonosc.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Teléfonos
					</a></li>
					<li><a class="" href="{{ route('direccionesc.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Direcciones
					</a></li>
				</ul>
			</li>
			<li><a href="{{ route('distribuidores.index')}}"><em class="fa fa-user">&nbsp;</em> Distribuidores</a></li>
			<li><a href="{{ route('pagos.index')}}"><em class="fa fa-credit-card">&nbsp;</em> Métodos de pago</a></li>
			
			<li>
            <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                                    Cerrar Sesión 
                    </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                </form>


            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		<!-- titulo-->
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">@yield('title')</h1>
			</div>
		</div><!--/.row-->
		@include('flash::message')
		@include('administrador.error')
		@yield('content')
			<div class="col-sm-12">
				<p class="back-link">Lumino Theme by <a href="https://www.medialoot.com">Medialoot</a></p>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->
	
	<!-- Scripts-->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>

    <script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('chosen/chosen.jquery.js') }}"></script>
	<script src="{{asset('js/chart.min.js')}}"></script>
	<script src="{{asset('js/chart-data.js')}}"></script>
	<script src="{{asset('js/easypiechart.js')}}"></script>
	<script src="{{asset('js/easypiechart-data.js')}}"></script>
	<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('js/custom.js')}}"></script>
	<script src="{{asset('js/jquery.rut.js')}}"></script>
	<script>
	window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
@yield('js')
</body>
</html>
