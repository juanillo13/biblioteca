@extends('administrador.main')

@section('title','Crear un nuevo estado')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nuevo estado</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'estados.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        {!! Form::label('estado','Estado')!!}
                                        {!! Form::text('estado',Null,['class' => 'form-control', 'placeholder' => 'ejemplo: Disponible', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection
