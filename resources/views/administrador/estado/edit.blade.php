@extends('administrador.main')

@section('title','Editar estado: '.$estado->estado )

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar estado</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => ['estados.update',$estado], 'method' => 'PUT']) !!}
                                    <div class="form-group">
                                        {!! Form::label('estado','Estado')!!}
                                        {!! Form::text('estado',$estado->estado,['class' => 'form-control', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection
