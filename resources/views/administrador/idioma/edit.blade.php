@extends('administrador.main')

@section('title','Editar Idioma: '.$idioma->idioma )

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar Idioma</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => ['idiomas.update',$idioma], 'method' => 'PUT']) !!}
                                    <div class="form-group">
                                        {!! Form::label('idioma','Idioma')!!}
                                        {!! Form::text('idioma',$idioma->idioma,['class' => 'form-control', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    

@endsection