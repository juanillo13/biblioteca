@extends('administrador.main')

@section('title','Mantenedor de Idiomas')

@section('content')

{!! Form::open(['route' => 'idiomas.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar Idioma..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
{!! Form::close() !!}

<div class="container">
	<a href="{{ route('idiomas.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Idiomas</a>
</div>	
</br>
<div class="panel panel-default">
	<div class="panel-heading">Idiomas</div>
		<div class="panel-body">
			<div class="col-md-12">
                <table class="table table-bordered">
                <thead>
                        <th class="col-md-2">ID</th>
                        <th>Nombre</th>
                        <th class="col-md-2">Acción</th>
                </thead>
                <tbody>
                        @foreach($idiomas as $idioma)
                            <tr>
                                <td>{{ $idioma->id }}</td>
                                <td>{{ $idioma->idioma }}</td>
   
                                <td> 
                                    <a href="{{route('idiomas.edit',$idioma->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('idiomas.destroy',$idioma->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $idiomas->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection
