
@extends('administrador.main')

@section('title','Mantenedor de Clientes')

@section('content')


    {!! Form::open(['route' => 'clientes.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar por nombre..', 'aria-describedby' =>'search']) !!}
		<span class="input-group-addon" id="search"><span class="fa fa-search"></span></span>
	</div>
    {!! Form::close() !!}
<div class="container">
	<a href="{{ route('clientes.create')}}" class="btn btn-info btn-lg"><i class="fa fa-plus"></i> Cliente</a>
</div>	
<div class="panel panel-default">
	<div class="panel-heading">Clientes</div>
		<div class="panel-body">
			<div class="col-md-12 table-responsive">
                <table class="table table-bordered">
                <thead>
                        <th>ID</th>
                        <th>RUT</th>
                        <th >Nombres</th>
                        <th>Apellidos</th>
                        <th >Fecha de Nacimiento</th>
                        <th >Acciones</th>

                </thead>
                <tbody>
                        @foreach($clientes as $cliente)
                            <tr>
                                <td>{{ $cliente->id }}</td>
                                <td>{{ $cliente->rut}}</td>
                                <td>{{ $cliente->nombre }}</td>
                                <td>{{ $cliente->apellido_paterno.' '.$cliente->apellido_materno}}</td>                    
                                <td>{{ $cliente->fecha_nacimiento }}</td>
                                
                                <td> 
                                    <a href="{{route('clientes.edit',$cliente->id)}}" class="btn btn-success"><i class="fa fa-pencil-square"></i></a>
                                    <a href="{{ route('clientes.destroy',$cliente->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                    
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $clientes->render() }}<!--es para a paginación-->
            </div>
        </div>
    </div>
@endsection

