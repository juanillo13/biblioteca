@extends('administrador.main')

@section('title','Editar cliente: '.$cliente->nombre.' '.$cliente->apellido_paterno)

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Editar cliente</div>
					<div class="panel-body">
						<div class="col-md-12">
                        {!! Form::open(['route' => ['clientes.update',$cliente], 'method' => 'PUT']) !!}
                        <div class="form-group">
                                        {!! Form::label('rut','RUT')!!}
                                        {!! Form::text('rut',$cliente->rut,['class' => ['form-control','rut'], 'placeholder' => '11.111.111-1', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('nombre','Nombre')!!}
                                        {!! Form::text('nombre',$cliente->nombre,['class' => 'form-control', 'placeholder' => 'Ambos nombres', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_paterno','Apellido Paterno')!!}
                                        {!! Form::text('apellido_paterno',$cliente->apellido_paterno,['class' => 'form-control', 'placeholder' => ''])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_materno','Apellido Materno')!!}
                                        {!! Form::text('apellido_materno',$cliente->apellido_materno,['class' => 'form-control', 'placeholder' => ''])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('fecha_nacimiento','Fecha de Nacimiento')!!}
                                        {!! Form::text('fecha_nacimiento',$cliente->fecha_nacimiento,['class' => 'form-control', 'placeholder' => ''])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Editar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

                        </div>
					</div>
				</div>
			</div><!-- /.panel-->

    @endsection


@section('js')

        <script type="text/javascript">
            
        $(".rut")
        .rut({formatOn: 'keyup', validateOn: 'keyup'})
        .on('rutInvalido', function(){ 
            $(this).parents(".control-group").addClass("error")
        })
        .on('rutValido', function(){ 
        $(this).parents(".control-group").removeClass("error")
        });
        </script>

@endsection
