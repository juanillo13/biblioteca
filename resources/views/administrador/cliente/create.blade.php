@extends('administrador.main')

@section('title','Crear un nuevo cliente')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Nuevo cliente</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'clientes.store', 'method' => 'POST']) !!}
                                <div class="form-group">
                                        {!! Form::label('rut','RUT')!!}
                                        {!! Form::text('rut',Null,['class' => ['form-control','rut'], 'placeholder' => '11.111.111-1', 'required'])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('nombre','Nombre')!!}
                                        {!! Form::text('nombre',Null,['class' => 'form-control', 'placeholder' => 'Ambos nombres', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_paterno','Apellido Paterno')!!}
                                        {!! Form::text('apellido_paterno',Null,['class' => 'form-control', 'placeholder' => ''])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('apellido_materno','Apellido Materno')!!}
                                        {!! Form::text('apellido_materno',Null,['class' => 'form-control', 'placeholder' => ''])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('fecha_nacimiento','Fecha de Nacimiento')!!}
                                        {!! Form::text('fecha_nacimiento',Null,['class' => 'form-control', 'placeholder' => ''])!!}
                                    </div>
                                    <div class="form-group">
                                        
                                        {!! Form::submit('Agregar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->
    
@endsection


@section('js')

<script type="text/javascript">
	
$(".rut")
  .rut({formatOn: 'keyup', validateOn: 'keyup'})
  .on('rutInvalido', function(){ 
    $(this).parents(".control-group").addClass("error")
  })
  .on('rutValido', function(){ 
$(this).parents(".control-group").removeClass("error")
  });
</script>

@endsection