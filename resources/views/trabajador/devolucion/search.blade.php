@extends('trabajador.main')

@section('title','Buscar arriendos de un cliente')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Buscar cliente</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'devoluciones.search', 'method' => 'POST']) !!}

                                    
                                    <div class="form-group">
			                            {!! Form::label('cliente_id', 'Clientes') !!}
			                            {!! Form::select('cliente_id', $clientes,NULL,['class' => 'form-control chosen-select','placeholder' => 'Seleccione una opción...','required'])!!}
		                            </div>

                                    <div class="form-group">
                                        
                                        {!! Form::submit('Buscar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->

@endsection

@section('js')

<script type="text/javascript">
	
	$(".chosen-select").chosen({

		no_results_text: "No se encontraron coincidencias!!"
	});
</script>

@endsection