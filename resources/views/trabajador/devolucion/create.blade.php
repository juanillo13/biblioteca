@extends('trabajador.main')

@section('title','Devolucion: '.$arriendo->libro->titulo)

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">arrendado por: {{$arriendo->cliente->nombre." ".$arriendo->cliente->apellido_paterno. "   fecha de arriendo: ".$arriendo->fecha_arriendo}}</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'devoluciones.store', 'method' => 'POST']) !!}

                                    <div class="form-group">
                                        {!! Form::label('arriendo_libro_id','Identificador de arriendo')!!}
                                        {!! Form::number('arriendo_libro_id',$arriendo->id,['class' => 'form-control','readonly' => 'true', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('multa','Multa')!!}
                                        {!! Form::number('multa',Null,['class' => 'form-control', 'placeholder' => 'se cobra una multa de $500 por día','required'])!!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('costo','Costo total')!!}
                                        {!! Form::number('costo_total',Null,['class' => 'form-control', 'placeholder' => 'Costo del arriendo mas la multa','required'])!!}
                                    </div>

                                     <div class="form-group">
                                        {!! Form::label('dias_retrado','Días de retraso')!!}
                                        {!! Form::number('dias_retraso',Null,['class' => 'form-control', 'placeholder' => '','required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('fecha','Fecha de devolucion')!!}
                                        {!! Form::text('fecha_devolucion',Null,['class' => 'form-control', 'placeholder' => 'click para ver calendario','data-provide="datepicker"', 'required'])!!}
                                    
                                    </div>

                                    <div class="form-group">
                                        
                                        {!! Form::submit('Devolver',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->  

@endsection