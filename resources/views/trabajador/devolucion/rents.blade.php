@extends('trabajador.main')

@section('title','Arriendos de '.$cliente->nombre.' '.$cliente->apellido_paterno)

@section('content')



<div class="panel panel-default">
	<div class="panel-heading">Arriendos</div>
		<div class="panel-body">
			<div class="col-md-12 table-responsive">
                <table class="table table-bordered">
                <thead>
                        <th>ID</th>
                        <th>Libro</th>
                        <th>Cliente</th>
                        <th>Costo arriendo</th>
                        <th>Fecha arriendo</th>
                        <th>fecha devolución</th>
                        <th>Trabajador</th>

                      <th class="col-sm-3">Acción</th>
                </thead>
                <tbody>
                       @foreach($arriendos as $arriendo)
                            <tr>
                                <td>{{ $arriendo->id }}</td>
                                
                                <td>{{ $arriendo->libro->titulo }}</td>
                              
                                <td>{{ $arriendo->cliente->nombre." ".$arriendo->cliente->apellido_paterno }}</td>
                                <td>{{ $arriendo->costo_arriendo }}</td>
                                <td>{{ $arriendo->fecha_arriendo }}</td>
                                <td>{{ $arriendo->fecha_devolucion_estimada }}</td>
                                <td>{{ $arriendo->trabajador->nombre." ".$arriendo->trabajador->apellido_paterno }}</td>

                                <td> 
                                <a href="{{route('devoluciones.edit',$arriendo->id)}}" class="btn btn-info">Devolución</a>
                        
                                </td>
                            </tr>

                        @endforeach
                </tbody>
                </table>
                {{ $arriendos->render() }}
                
            </div>
        </div>
    </div>
@endsection