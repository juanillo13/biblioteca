@extends('trabajador.main')

@section('title','Arrendar')

@section('content')

    		<div class="panel panel-default">
				<div class="panel-heading">Crear Arriendo</div>
					<div class="panel-body">
						<div class="col-md-12">
                                {!! Form::open(['route' => 'arriendos.store', 'method' => 'POST']) !!}
                                    <div class="form-group">
			                            {!! Form::label('libro_id', 'Libros') !!}
			                            {!! Form::select('libro_id', $libros,NULL,['class' => 'form-control','placeholder' => 'Seleccione una opción...','required'])!!}
		                            </div>
                                    
                                    <div class="form-group">
			                            {!! Form::label('cliente_id', 'Clientes') !!}
			                            {!! Form::select('cliente_id', $clientes,NULL,['class' => 'form-control','placeholder' => 'Seleccione una opción...','required'])!!}
		                            </div>

                                    <div class="form-group">
                                        {!! Form::label('costo_arriendo','Precio del Arriendo')!!}
                                        {!! Form::number('costo_arriendo',Null,['class' => 'form-control', 'placeholder' => 'ejemplo: 15000', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('fecha','Fecha de arriendo')!!}
                                        {!! Form::text('fecha_arriendo',Null,['class' => 'form-control', 'placeholder' => 'click para ver calendario','data-provide="datepicker"', 'required'])!!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('fecha','Fecha de devolucion')!!}
                                        {!! Form::text('fecha_devolucion_estimada',Null,['class' => 'form-control', 'placeholder' => 'click para ver calendario','data-provide="datepicker"', 'required'])!!}
                                    
                                    </div>

                                    <div class="form-group">
			                            {!! Form::label('trabajador_id', 'Trabajadores') !!}
			                            {!! Form::select('trabajador_id', $trabajadores,NULL,['class' => 'form-control','placeholder' => 'Seleccione una opción...','required'])!!}
		                            </div>



                                    <div class="form-group">
                                        
                                        {!! Form::submit('Arrendar',['class' => 'btn btn-success btn-lg'])!!}

                                    </div>		

                                {!! Form::close() !!}

							
                        </div>
					</div>
				</div>
			</div><!-- /.panel-->

    <script type="text/javascript">

	 $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose:true
           
        });
    </script>
    

@endsection