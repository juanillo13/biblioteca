@extends('trabajador.main')
@section('title','Dashboard')

@section('content')

<div class="panel panel-container">
			<div class="row">
				<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
					<div class="panel panel-teal panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-book color-blue"></em>
							<div class="large"></div>
							<div class="text-muted">Libros</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
					<div class="panel panel-blue panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-user color-orange"></em>
							<div class="large"></div>
							<div class="text-muted">Clientes</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
					<div class="panel panel-orange panel-widget border-right">
						<div class="row no-padding"><em class="fa fa-xl fa-users color-teal"></em>
							<div class="large"></div>
							<div class="text-muted">Usuarios</div>
						</div>
					</div>
				</div>
				<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-user color-orange"></em>
							<div class="large"></div>
							<div class="text-muted">Distribuidores</div>
						</div>
					</div>
				</div>
			</div><!--/.row-->
        </div>

	</div><!--/.col-->

@endsection