<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrarTablaAutor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido_paterno');
            $table->string('apellido_materno');
            #$table->integer('libro_id')->unsigned();
            #$table->foreign('libro_nserie')->references('n_serie')->on('libro');
            #$table->foreign('libro_id')->references('id')->on('libro')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::create('autor_libro', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('libro_id')->unsigned();
            $table->integer('autor_id')->unsigned();
            $table->timestamps();
            $table->foreign('libro_id')->references('id')->on('libro');
            $table->foreign('autor_id')->references('id')->on('autor');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autor');
    }
}
