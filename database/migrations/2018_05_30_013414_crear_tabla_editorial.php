<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaEditorial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('editorial', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nombre_editorial');
            #$table->integer('libro_id')->unsigned();
            #$table->foreign('libro_id')->references('id')->on('libro')->onDelete('cascade');
            $table->timestamps();
        });

        #tabla pivote

        Schema::create('editorial_libro', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('libro_id')->unsigned();
            $table->integer('editorial_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('libro_id')->references('id')->on('libro');
            $table->foreign('editorial_id')->references('id')->on('editorial');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('editorial');
    }
}
