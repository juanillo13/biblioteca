<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaDireccionT extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccionT', function (Blueprint $table) {
            $table->increments('id');
            $table->string('direccion');
            $table->integer('trabajador_id')->unsigned();

            $table->foreign('trabajador_id')->references('id')->on('trabajador')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direccionT');
    }
}
