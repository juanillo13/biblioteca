<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaIdioma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idioma', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idioma');
            #$table->integer('libro_id')->unsigned();
            #$table->foreign('libro_id')->references('id')->on('libro')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::create('idioma_libro', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('libro_id')->unsigned();
            $table->integer('idioma_id')->unsigned();
            $table->timestamps();
            $table->foreign('libro_id')->references('id')->on('libro');
            $table->foreign('idioma_id')->references('id')->on('idioma');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idioma');
    }
}
