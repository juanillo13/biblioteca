<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaDetalleCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_compra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('precio_neto');
            $table->integer('precio_iva');
            $table->integer('costo_iva');
            $table->integer('factura_id')->unsigned();
            $table->integer('libro_id')->unsigned();

            $table->foreign('factura_id')->references('id')->on('factura')->onDelete('cascade');
            $table->foreign('libro_id')->references('id')->on('libro')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_compra');
    }
}
