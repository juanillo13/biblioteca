<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaLibro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libro', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isbn');
            $table->text('titulo');
            $table->integer('n_paginas');
            $table->integer('p_refrencia');
            $table->integer('año_publicacion');

            #para asignar las claves foraneas tenemos que hacer lo siguiente!!
            $table->integer('estado_id')->unsigned();
            $table->foreign('estado_id')->references('id')->on('estado')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libro');
    }
}
