<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaArriendoLibro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arriendo_libro', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('libro_id')->unsigned();
            $table->integer('cliente_id')->unsigned();
            $table->integer('costo_arriendo');
            $table->string('fecha_arriendo');
            $table->string('fecha_devolucion_estimada');
            $table->integer('trabajador_id')->unsigned();
            $table->timestamps();

            $table->foreign('libro_id')->references('id')->on('libro')->onDelete('cascade');
            $table->foreign('cliente_id')->references('id')->on('cliente')->onDelete('cascade');
            $table->foreign('trabajador_id')->references('id')->on('trabajador')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arriendo_libro');
    }
}
