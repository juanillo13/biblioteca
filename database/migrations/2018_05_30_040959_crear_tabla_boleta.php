<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaBoleta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boleta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hora_venta');
            $table->string('fecha_compra');
            $table->integer('cliente_id')->unsigned();
            $table->integer('trabajador_id')->unsigned();
            $table->integer('metodo_pago_id')->unsigned();

            $table->foreign('cliente_id')->references('id')->on('cliente')->onDelete('cascade');
            $table->foreign('trabajador_id')->references('id')->on('trabajador')->onDelete('cascade');
            $table->foreign('metodo_pago_id')->references('id')->on('metodo_pago')->onDetelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boleta');
    }
}
