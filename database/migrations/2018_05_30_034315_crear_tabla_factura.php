<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hora_compra');
            $table->string('fecha_compra');
            $table->integer('distribuidor_id')->unsigned();
            $table->integer('metodo_pago_id')->unsigned();

            $table->foreign('distribuidor_id')->references('id')->on('distribuidor')->onDelete('cascade');
            $table->foreign('metodo_pago_id')->references('id')->on('metodo_pago')->onDetelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura');
    }
}
