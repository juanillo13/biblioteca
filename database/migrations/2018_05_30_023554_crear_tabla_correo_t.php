<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaCorreoT extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correoT', function (Blueprint $table) {
            $table->increments('id');
            $table->string('correo');
            $table->integer('trabajador_id')->unsigned();

            $table->foreign('trabajador_id')->references('id')->on('trabajador')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('correoT');
    }
}
