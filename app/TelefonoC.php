<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class TelefonoC extends Model
{
    use notifiable;
    protected $table = "telefonoC";

    protected $fillable = [
        'id',
        'telefono',
        'cliente_id'
    ];

    public function cliente(){

        return $this->belongsTo('App\Cliente');
    }

                #función que permite crear un buscador usando SCOPE
                public function scopeSearch($query, $name) #segundo parámetro es el que se manda

                {
                    return $query->where('telefono','LIKE',"%$name%");
            
                }
                    public function scopeSearchTag($query, $name)
                {
            
                    return $query->where('cliente_id','=',$name);
                }

}
