<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class CorreoT extends Model
{
    use notifiable;

    protected $table = "correoT";

    protected $fillable =[
        'id',
        'correo',
        'trabajador_id'
    ];

    public function trabajador(){

        return $this->belongsTo('App\Trabajador');
    }
 #función que permite crear un buscador usando SCOPE
            public function scopeSearch($query, $name) #segundo parámetro es el que se manda

            {
                return $query->where('correo','LIKE',"%$name%");
        
            }
                public function scopeSearchTag($query, $name)
            {
        
                return $query->where('correo','=',$name);
            }

}
