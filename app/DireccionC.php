<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class DireccionC extends Model
{
    use notifiable;

    protected $table = "direccionC";

    protected $fillable = [
        'id',
        'direccion',
        'cliente_id'
    ];

    public function cliente(){

        return $this->belongsTo('App\Cliente');
    }

    
     #función que permite crear un buscador usando SCOPE
     public function scopeSearch($query, $name) #segundo parámetro es el que se manda

     {
         return $query->where('direccion','LIKE',"%$name%");
 
     }
         public function scopeSearchTag($query, $name)
     {
 
         return $query->where('direccion','=',$name);
     }
}
