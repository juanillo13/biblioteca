<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    use notifiable;
    protected $table = "factura";

    protected $fillable =[
        'id',
        'hora_compra',
        'fecha_compra',
        'distribuidor_id',
        'metodo_pago_id'
    ];

    #protected $hidden = ['id'];


    public function distribuidor(){

        return $this->belongsTo('App\Distribuidor');
    }

    #nota mental: belongsTo va en el modelo que tenga 
    #el atributo en la base de datos en una relacion 1-1
    public function metodo_pago(){

        return $this->belongsTo('App\Metodo_pago');

    }
    public function detalles_compras(){

        return $this->hasMany('App\Detalle_compra');
    }
}
