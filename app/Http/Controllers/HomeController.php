<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libro;
use App\User;
use App\Cliente;
use App\Distribuidor;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $libros= Libro::get()->toArray();
        $cuenta_libros = count($libros);

        $usuarios= User::get()->toArray();
        $cuenta_usuarios = count($usuarios);

        $clientes= Cliente::get()->toArray();
        $cuenta_clientes = count($clientes);

        $distribuidores= Distribuidor::get()->toArray();
        $cuenta_distribuidores = count($distribuidores);

        return view('administrador.index',[
            'cuenta_libros' => $cuenta_libros,
            'cuenta_usuarios' => $cuenta_usuarios,
            'cuenta_clientes' => $cuenta_clientes,
            'cuenta_distribuidores' => $cuenta_distribuidores
            ]);
       

    }
    public function index2()
    {   
        
        return view('trabajador.index');

    }
}
