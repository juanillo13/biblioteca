<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TelefonoC;
use App\Cliente;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestTelefonoC;

class ControladorTelefonoC extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $telefonos= TelefonoC::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.telefonoC.index',['telefonos' => $telefonos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Cliente::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.telefonoC.create',['clientes' => $clientes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestTelefonoC $request)
    {
        $telefono = new TelefonoC($request->all());
        $telefono->save();
        Flash::success('se a creado teléfono '.$telefono->telefono.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('telefonosc.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $telefono = TelefonoC::find($id);
        $telefono->cliente;
        $clientes = Cliente::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.telefonoC.edit',['clientes' => $clientes, 'telefono' => $telefono]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $telefono = TelefonoC::find($id);
        $telefono->fill($request->all());
        $telefono->save();
        Flash::warning('se ha editado el teléfono '.$telefono->telefono.' de forma exitosa!!!');
        return redirect()->route('telefonosc.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $telefono = TelefonoC::find($id);
        $telefono->delete();

        Flash::error('se ha eliminado el teléfono '.$telefono->telefono.' de forma exitosa!!!');
        return redirect()->route('telefonosc.index');
    }
    
}
