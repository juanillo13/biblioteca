<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DireccionT;
use App\Trabajador;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestDireccionT;

class ControladorDireccionT extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $direcciones= DireccionT::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.direccionT.index',['direcciones' => $direcciones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trabajadores = Trabajador::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.direccionT.create',['trabajadores' => $trabajadores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestDireccionT $request)
    {
        $direccion = new DireccionT($request->all());
        $direccion->save();
        Flash::success('se a creado '.$direccion->direccion.' de forma exitosa');
       
        return redirect()->route('direccionest.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $direccion = DireccionT::find($id);
        $direccion->trabajador;
        $trabajadores = Trabajador::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.direccionT.edit',['trabajadores' => $trabajadores, 'direccion' => $direccion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $direccion = DireccionT::find($id);
        $direccion->fill($request->all());
        $direccion->save();
        Flash::warning('se ha editado la dirección '.$direccion->direccion.' de forma exitosa!!!');
        return redirect()->route('direccionest.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $direccion = DireccionT::find($id);
        $direccion->delete();

        Flash::error('se ha eliminado la dirección '.$direccion->direccion.' de forma exitosa!!!');
        return redirect()->route('direccionest.index');
    }
}
