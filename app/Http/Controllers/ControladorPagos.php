<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Metodo_pago;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestPago;

class ControladorPagos extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pagos= Metodo_pago::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.metodo_pago.index',['pagos' => $pagos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.metodo_pago.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestPago $request)
    {
        #ruta: pagos
        $pago = new Metodo_pago($request->all());
        $pago->save();
        Flash::success('se a creado '.$pago->metodo_pago.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('pagos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pago = Metodo_pago::find($id);
        return view('administrador.metodo_pago.edit',['pago' => $pago]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pago = Metodo_pago::find($id);
        $pago->metodo_pago = $request->metodo_pago;
        $pago->save();
        
        Flash::warning('El método de pago '.$pago->metodo_pago.' ha sido modificado exitosamente')->important();
        return redirect()->route('pagos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pago = Metodo_pago::find($id);
        $pago->delete();
        Flash::error('Se ha eliminado '. $pago->metodo_pago. ' de forma exitosa');
        return redirect()->route('pagos.index');

    }
}
