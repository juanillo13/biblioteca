<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estado;
use App\Categoria;
use App\Autor;
use App\Idioma;
use App\Editorial;
use App\Libro;

use Laracasts\Flash\Flash;
use App\Http\Requests\RequestLibro;



class ControladorLibros extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $libros= Libro::search($request->name)->orderBy('id','DESC')->paginate(6);
    	#llamar relaciones each hace un recorrido por cada de uno de los articulos, despues llamo a las relaciones para obtener los datos de la categoria y el usuario del articulo.
    	$libros->each(function($libros){
            $libros->estado;
            $libros->autores;
            $libros->idiomas;
            $libros->categorias;
            $libros->editoriales;
        });
       
    	
    	return view('administrador.libro.index',['libros'=> $libros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = Estado::orderBy('estado','ASC')->pluck('estado','id');
        $categorias = Categoria::orderBy('categoria','ASC')->pluck('categoria','id');
        $autores = Autor::orderBy('id','ASC')->pluck('nombre','id'); 
        $idiomas = Idioma::orderBy('idioma','ASC')->pluck('idioma','id');
        $editoriales = Editorial::orderBy('nombre_editorial','ASC')->pluck('nombre_editorial','id');   
        #pluck es un método que permite listar un objeto por los parametros que se le pasen.
        return view('administrador.libro.create',[
            'estados' =>$estados, 
            'categorias' =>$categorias,
            'autores' =>$autores,
            'idiomas' =>$idiomas,
            'editoriales' =>$editoriales,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestLibro $request)
    {
        $libro = new Libro($request->all());
        $libro->save();

        $libro->editoriales()->sync($request->editoriales);
        $libro->categorias()->sync($request->categorias);
        $libro->idiomas()->sync($request->idiomas);
        $libro->autores()->sync($request->autores);

        Flash::success('se ha creado el libro ' . $libro->titulo . ' de manera exitosa!!');
    	return redirect()->route('libros.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $libro = Libro::find($id);
        $libro->estado; #llamo a la relacion estado para traer el estado de un libro

        #traigo a todas las todos los registros de las diferntes tablas para enviarlos a la vista
        #para luego ser seleccionados
        $estados = Estado::orderBy('estado','ASC')->pluck('estado','id');
        $categorias = Categoria::orderBy('categoria','ASC')->pluck('categoria','id');
        $autores = Autor::orderBy('id','ASC')->pluck('nombre','id'); 
        $idiomas = Idioma::orderBy('idioma','ASC')->pluck('idioma','id');
        $editoriales = Editorial::orderBy('nombre_editorial','ASC')->pluck('nombre_editorial','id'); 

        #ademas tengo que traer las relaciones que ya he guardado del libro que voy a editar
        #por las relaciones muchos a muchos
        $mis_categorias = $libro->categorias->pluck('id')->ToArray();
        $mis_autores = $libro->autores->pluck('id')->ToArray();
        $mis_idiomas = $libro->idiomas->pluck('id')->ToArray();
        $mis_editoriales = $libro->editoriales->pluck('id')->ToArray();

        return view('administrador.libro.edit',[
            'estados'=>         $estados,
            'categorias' =>$categorias,
            'autores' =>$autores,
            'idiomas' =>$idiomas,
            'editoriales' =>$editoriales,
            'libro'=>           $libro,
            'mis_categorias'=>  $mis_categorias,
            'mis_autores' =>    $mis_autores,
            'mis_idiomas'=>     $mis_idiomas,
            'mis_editoriales' =>$mis_editoriales
            ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestLibro $request, $id)
    {
        $libro = Libro::find($id);
        #vamos a usar el metodo fill que hace coincidir los campos con los enviados del formulario
        $libro->fill($request->all());
        $libro->save();

        #rellenamos las tablas pivotes
        $libro->editoriales()->sync($request->editoriales);
        $libro->categorias()->sync($request->categorias);
        $libro->idiomas()->sync($request->idiomas);
        $libro->autores()->sync($request->autores);

        Flash::warning('se ha editado el libro '.$libro->titulo.' de forma exitosa!!!');
        return redirect()->route('libros.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $libro = Libro::find($id);
        $libro->delete();

        Flash::error('se ha eliminado el libro '.$libro->titulo.' de forma exitosa!!!');
        return redirect()->route('libros.index');

    }
}
