<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Autor;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestAutor;

class ControladorAutores extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $autores= Autor::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.autor.index',['autores' => $autores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.autor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestAutor $request)
    {
        $autor = new Autor($request->all());
        $autor->save();
        Flash::success('se a creado '.$autor->nombre.' '.$autor->apellido_paterno.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('autores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $autor = Autor::find($id);
        return view('administrador.autor.edit',['autor' => $autor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestAutor $request, $id)
    {
        $autor = Autor::find($id);
        $autor->nombre = $request->nombre;
        $autor->apellido_paterno = $request->apellido_paterno;
        $autor->apellido_materno = $request->apellido_materno;
        $autor->save();
        Flash::warning('El autor '.$autor->nombre.' '.$autor->apellido_paterno.' ha sido modificado exitosamente');
        return redirect()->route('autores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $autor = Autor::find($id);
        $autor->delete();

        Flash::error('Se ha eliminado '.$autor->nombre.' '.$autor->apellido_paterno.' de forma exitosa');
        return redirect()->route('autores.index');
    }
}
