<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TelefonoT;
use App\Trabajador;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestTelefonoT;

class ControladorTelefonoT extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $telefonos= TelefonoT::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.telefonoT.index',['telefonos' => $telefonos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trabajadores = Trabajador::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.telefonoT.create',['trabajadores' => $trabajadores]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestTelefonoT $request)
    {
        $telefono = new TelefonoT($request->all());
        $telefono->save();
        Flash::success('se a creado teléfono '.$telefono->telefono.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('telefonost.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $telefono = TelefonoT::find($id);
        $telefono->trabajador;
        $trabajadores = Trabajador::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.telefonoT.edit',['trabajadores' => $trabajadores, 'telefono' => $telefono]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $telefono = TelefonoT::find($id);
        $telefono->fill($request->all());
        $telefono->save();
        Flash::warning('se ha editado el teléfono '.$telefono->telefono.' de forma exitosa!!!');
        return redirect()->route('telefonost.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $telefono = TelefonoT::find($id);
        $telefono->delete();

        Flash::error('se ha eliminado el teléfono '.$telefono->telefono.' de forma exitosa!!!');
        return redirect()->route('telefonost.index');
    }
}
