<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Cliente;
use App\Libro;
use App\Trabajador;
use App\Arriendo_libro;
use App\devolver_libro;



class ControladorDevolucion extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //aqui vamos a tener la tabla con todos los arriendos de un cliente
        $clientes = Cliente::orderBy('nombre','ASC')->pluck('nombre','id');
       
        return view('trabajador.devolucion.search',['clientes' => $clientes]);

    }
    #es una ruta tipo post! debo enviar un dato por un formulario post tiene request y deve devolver una ruta
    public function search(Request $request)
    {
        
        #$cliente = Cliente::find($request->cliente_id);
        $cliente_id = $request->cliente_id;
        #llamo la relacion 

        #return view('trabajador.devolucion.rents',['cliente' => $cliente]);
        return redirect()->route('devoluciones.results',$cliente_id);
       # dd($cliente);

        

    }

    public function results($id){
        $cliente = Cliente::find($id);
        $cliente->arriendos;
        

        #otra prueba 
        
        $arriendos = Arriendo_libro::where('cliente_id','=',$id)->orderBy('id','DESC')->paginate(6);
        $arriendos->each(function($arriendos){
            $arriendos->libro;
            $arriendos->trabajador;
            $arriendos->cliente;
        });
    
       
        return view('trabajador.devolucion.rents',[
            'cliente' => $cliente,
            'arriendos' =>$arriendos
            ]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $devolucion = new Devolver_libro($request->all());
        $arriendo = Arriendo_libro::find($request->arriendo_libro_id);
        $cliente_id = $arriendo->cliente_id;
        $libro = Libro::find($arriendo->libro_id);
        $libro->estado_id = 1;
        $libro->save();
        $devolucion->save();
        $arriendo->delete();
        Flash::success('se a registrado la devolución del libro  '.$libro->titulo.' de forma exitosa');
        return redirect()->route('devoluciones.results',$cliente_id);
    }

    /**
     * Display the specified resource.
     *
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     /* voy a usar la ruta edit de tipo post para obtener el id y cargar una vista */
    public function edit($id)
    {
        //voy a obtener el id de un arriendo 
        $arriendo = Arriendo_libro::find($id);
        return view('trabajador.devolucion.create',['arriendo' => $arriendo]);
        //dd($arriendo);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
