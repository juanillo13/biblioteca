<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CorreoT;
use App\Trabajador;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestCorreoT;


class ControladorCorreoT extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $correos= CorreoT::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.correoT.index',['correos' => $correos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trabajadores = Trabajador::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.correot.create',['trabajadores' => $trabajadores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestCorreoT $request)
    {
        $correo = new CorreoT($request->all());
        $correo->save();
        Flash::success('se a creado '.$correo->correo.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('correost.index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $correo = CorreoT::find($id);
        $correo->trabajador;
        $trabajadores = Trabajador::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.correot.edit',['trabajadores' => $trabajadores, 'correo' => $correo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $correo = CorreoT::find($id);
        $correo->fill($request->all());
        $correo->save();
        Flash::warning('se ha editado el correo '.$correo->correo.' de forma exitosa!!!');
        return redirect()->route('correost.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $correo = CorreoT::find($id);
        $correo->delete();

        Flash::error('se ha eliminado el correo '.$correo->correo.' de forma exitosa!!!');
        return redirect()->route('correost.index');
    }
}
