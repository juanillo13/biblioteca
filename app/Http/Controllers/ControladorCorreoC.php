<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CorreoC;
use App\Cliente;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestCorreoC;

class ControladorCorreoC extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $correos= CorreoC::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.correoC.index',['correos' => $correos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Cliente::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.correoC.create',['clientes' => $clientes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestCorreoC $request)
    {
        $correo = new CorreoC($request->all());
        $correo->save();
        Flash::success('se a creado '.$correo->correo.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('correosc.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $correo = CorreoC::find($id);
        $correo->cliente;
        $clientes = Cliente::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.correoc.edit',['clientes' => $clientes, 'correo' => $correo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $correo = CorreoC::find($id);
        $correo->fill($request->all());
        $correo->save();
        Flash::warning('se ha editado el correo '.$correo->correo.' de forma exitosa!!!');
        return redirect()->route('correosc.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $correo = CorreoC::find($id);
        $correo->delete();

        Flash::error('se ha eliminado el correo '.$correo->correo.' de forma exitosa!!!');
        return redirect()->route('correosc.index');
    }
}
