<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DireccionC;
use App\Cliente;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestDireccionC;

class ControladorDireccionC extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $direcciones= DireccionC::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.direccionC.index',['direcciones' => $direcciones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Cliente::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.direccionC.create',['clientes' => $clientes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestDireccionC $request)
    {
        $direccion = new DireccionC($request->all());
        $direccion->save();
        Flash::success('se a creado '.$direccion->direccion.' de forma exitosa');
       
        return redirect()->route('direccionesc.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $direccion = DireccionC::find($id);
        $direccion->cliente;
        $clientes = Cliente::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('administrador.direccionC.edit',['clientes' => $clientes, 'direccion' => $direccion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $direccion = DireccionC::find($id);
        $direccion->fill($request->all());
        $direccion->save();
        Flash::warning('se ha editado la dirección '.$direccion->direccion.' de forma exitosa!!!');
        return redirect()->route('direccionesc.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $direccion = DireccionC::find($id);
        $direccion->delete();

        Flash::error('se ha eliminado la dirección '.$direccion->direccion.' de forma exitosa!!!');
        return redirect()->route('direccionesc.index');
    }
}
