<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trabajador;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestTrabajador;


class ControladorTrabajadores extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $trabajadores= Trabajador::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.trabajador.index',['trabajadores' => $trabajadores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.trabajador.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestTrabajador $request)
    {
        $trabajador = new Trabajador($request->all());
        $trabajador->save();
        Flash::success('se a creado '.$trabajador->nombre.' '.$trabajador->apellido_paterno.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('trabajadores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trabajador = Trabajador::find($id);
        return view('administrador.trabajador.edit',['trabajador' => $trabajador]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestTrabajador $request, $id)
    {
        $trabajador = Trabajador::find($id);
        $trabajador->fill($request->all());
        $trabajador->save();
        Flash::warning('El trabajador '.$trabajador->nombre.' '.$trabajador->apellido_paterno.' ha sido modificado exitosamente');
        return redirect()->route('trabajadores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trabajador = Trabajador::find($id);
        $trabajador->delete();

        Flash::error('El trabajador '.$trabajador->nombre.' '.$trabajador->apellido_paterno.' ha sido eliminado exitosamente');
        return redirect()->route('trabajadores.index');
    }
}
