<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Editorial;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestEditorial;

class ControladorEditoriales extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $editoriales= Editorial::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.editorial.index',['editoriales' => $editoriales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.editorial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestEditorial $request)
    {
        $editorial = new Editorial($request->all());
        $editorial->save();
        Flash::success('se a creado '.$editorial->nombre_editorial.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('editoriales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editorial = Editorial::find($id);
        
        return view('administrador.editorial.edit',['editorial' => $editorial]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestEditorial $request, $id)
    {
        $editorial = Editorial::find($id);
        $editorial->nombre_editorial = $request->nombre_editorial;
        $editorial->save();
        Flash::warning('La editorial '.$editorial->nombre_editorial.' ha sido modificado exitosamente');
        return redirect()->route('editoriales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $editorial = Editorial::find($id);
        $editorial->delete();

        Flash::error('Se ha eliminado '. $editorial->nombre_editorial. ' de forma exitosa');
        return redirect()->route('editoriales.index');
    }
}
