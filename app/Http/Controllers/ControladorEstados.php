<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estado;

use Laracasts\Flash\Flash;
use App\Http\Requests\RequestEstado;

class ControladorEstados extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $estados= Estado::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.estado.index',['estados' => $estados]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.estado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestEstado $request)
    {
        $estado = new Estado($request->all());
        $estado->save();
        Flash::success('se a creado '.$estado->estado.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('estados.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estado= Estado::find($id);
    
        return view ('administrador.estado.edit',['estado' => $estado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestEstado $request, $id)
    {
        $estado = Estado::find($id);
        $estado->estado = $request->estado;
        $estado->save();
        Flash::warning('El estado '.$estado->estado.' ha sido modificado exitosamente');
        return redirect()->route('estados.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estado = Estado::find($id);
        $estado->delete();

        Flash::error('Se ha eliminado '. $estado->estado. ' de forma exitosa');
        return redirect()->route('estados.index');
    }
}
