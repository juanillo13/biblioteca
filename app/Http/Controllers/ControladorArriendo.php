<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Libro;
use App\Cliente;
use App\Trabajador;
use App\arriendo_libro;
use Laracasts\Flash\Flash;

class ControladorArriendo extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #este método será un formulario para crear un formulario
        #debo traer solo los libros disponibles
        #$libros = DB::table('libro')->where('estado_id','=',1)->get();
        #es lo mismo que arriba pero asi no me tira error
        $libros = Libro::where('estado_id','=','1')->pluck('titulo','id');
        #debo traer a los clientes
        $clientes = Cliente::orderBy('nombre','ASC')->pluck('nombre','id');
        #debo traer a los trabajadores 
        $trabajadores = Trabajador::orderBy('nombre','ASC')->pluck('nombre','id');
        return view('trabajador.arrendar.create',[
            'libros' => $libros,
            'clientes' => $clientes,
            'trabajadores' => $trabajadores
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #traigo todo el objeto desde el formulario
        $arriendo = new Arriendo_libro($request->all());
        #en la variable libro le asigno el objeto Libro de id igual al id del libro que traigo en el request
        $libro = Libro::find($request->libro_id);
        #seteo el estado del libro en 3 que es arrendado
        $libro->estado_id = 3;
        $libro->save();
        $arriendo->save();
        Flash::success('se a registrado el arriendo  '.$arriendo->id.' de forma exitosa');
       
        return redirect()->route('arriendos.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
