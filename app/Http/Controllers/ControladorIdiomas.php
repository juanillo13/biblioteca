<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Idioma;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestIdioma;

class ControladorIdiomas extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $idiomas= Idioma::search($request->name)->orderBy('id','DESC')->paginate(6);
        #debo indicar la ruta donde estan el archivo de la vista
        return view('administrador.idioma.index',['idiomas' => $idiomas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.idioma.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestIdioma $request)
    {
        $idioma = new Idioma($request->all());
        $idioma->save();
        Flash::success('se a creado '.$idioma->idioma.' de forma exitosa');
        #tengo que hacer un return, pero lo haré mas adelante
        return redirect()->route('idiomas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idioma = Idioma::find($id);
        return view('administrador.idioma.edit',['idioma' => $idioma]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $idioma = idioma::find($id);
        $idioma->idioma = $request->idioma;
        $idioma->save();
        Flash::warning('El idioma '.$idioma->idioma.' ha sido modificado exitosamente');
        return redirect()->route('idiomas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idioma = idioma::find($id);
        $idioma->delete();

        Flash::error('Se ha eliminado '. $idioma->idioma. ' de forma exitosa');
        return redirect()->route('idiomas.index');
    }
}
