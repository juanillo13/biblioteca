<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libro;
use App\Categoria;

class ControladorPrueba extends Controller
{
    public function traerlibro($id){

        $libro = Libro::find($id);
        #$libro = $libro->categorias;
        $categorias = Categoria::orderBy('categoria','DESC')->pluck('categoria','id');

    	$libro_categorias = $libro->categorias->pluck('id')->ToArray();

        #retornar una vista!!!
                    #carpeta.nombredelavista
        return view('probando.index',['libro'=>$libro,'libro_categorias'=>$libro_categorias]); #tengo un array asociativo clave => valor, la clave será el nombre que se usa en la vista  




    }
}
