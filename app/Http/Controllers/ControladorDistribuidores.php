<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Distribuidor;
use Laracasts\Flash\Flash;
use App\Http\Requests\RequestDistribuidor;

class ControladorDistribuidores extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $distribuidores = Distribuidor::search($request->name)->orderBy('id','DESC')->paginate(6);
        return view('administrador.distribuidor.index',['distribuidores' => $distribuidores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administrador.distribuidor.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestDistribuidor $request)
    {
        $distribuidor = new Distribuidor($request->all());
        $distribuidor->save();
        Flash::success('se a creado '.$distribuidor->nombre_empresa.' de forma exitosa');
        return redirect()->route('distribuidores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $distribuidor = Distribuidor::find($id);
        return view('administrador.distribuidor.edit',['distribuidor' => $distribuidor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $distribuidor = Distribuidor::find($id);
        $distribuidor->fill($request->all());
        $distribuidor->save();

        Flash::warning('El distribuidor '.$distribuidor->nombre_empresa.' ha sido modificado exitosamente');
        return redirect()->route('distribuidores.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $distribuidor = Distribuidor::find($id);
        $distribuidor->delete();
        Flash::error('El distribuidor '.$distribuidor->nombre_empresa.' ha sido eliminado exitosamente');
        return redirect()->route('distribuidores.index');


    }
}
