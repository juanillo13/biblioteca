<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Libro;

class ControladorInicio extends Controller
{
    

    public function index ()
    {
        $categorias=Categoria::orderBy('id','DESC')->paginate(5);
        $libros=Libro::orderBy('id','DESC')->get();
        return view ('visitante.index',['categorias' => $categorias,'libros' => $libros]);
    }

}
