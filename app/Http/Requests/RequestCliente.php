<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestCliente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rut' =>    'min:10|max:120|required|unique:cliente',
            'nombre' => 'min:4|max:120|required',
            'apellido_paterno' => 'max:120|required',
            'apellido_materno' => 'max:120|required',
            'fecha_nacimiento' => 'min:4|max:120|required',
        ];
    }
}
