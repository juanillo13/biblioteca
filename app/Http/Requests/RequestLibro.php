<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestLibro extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'isbn   ' => 'min:10|max:120',
            'titulo   ' => 'min:4|max:500',
            'n_paginas   ' => 'min:0',
            'p_refrencia   ' => 'min:0|numeric',
            'año_publicacion' => 'min:0|numeric',
       

        ];
    }
}
