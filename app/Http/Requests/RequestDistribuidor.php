<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestDistribuidor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rut' =>    'min:10|max:120|required|unique:distribuidor',
            'nombre_empresa' => 'min:4|max:120|required',
            'direccion' => 'min:4|max:120|required',
            'telefono' => 'min:9|max:120|required',
            'anio_venta_libro' => 'max:120|required',
        ];
    }
}
