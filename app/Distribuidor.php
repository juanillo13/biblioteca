<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Distribuidor extends Model
{
    use notifiable;
    protected $table = "distribuidor";

    protected $fillable = [
        'id',
        'rut',
        'nombre_empresa',
        'direccion',
        'telefono',
        'anio_venta_libro'
    ];
    #protected $hidden = ['id'];

    public function facturas(){

        return $this->hasMany('App\Factura');
    }

     #función que permite crear un buscador usando SCOPE
     public function scopeSearch($query, $name) #segundo parámetro es el que se manda

     {
         return $query->where('nombre_empresa','LIKE',"%$name%");
 
     }
         public function scopeSearchTag($query, $name)
     {
 
         return $query->where('nombre_empresa','=',$name);
     }

}
