<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    use notifiable;


    protected $table = "estado";

    protected $fillable = ['id','estado'];

    #protected $hidden = ['id'];


    #relaciones 1-1 con libro

    public function libro(){

        return $this->hasOne('App\Libro');

    }

    #función que permite crear un buscador usando SCOPE
    public function scopeSearch($query, $name) #segundo parámetro es el que se manda

    {
        return $query->where('estado','LIKE',"%$name%");

    }
        public function scopeSearchTag($query, $name)
    {

        return $query->where('estado','=',$name);
    }
}
