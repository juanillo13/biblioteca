<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{

    use Notifiable;
    protected $table = "libro";
   # protected $primary_key = "n_serie";
    #protected $incrementing = false;

    protected $fillable = [

        'id',
        'isbn',
        'titulo',
        'n_paginas',
        'p_refrencia',
        'año_publicacion',
        'estado_id'
    ];
    #protected $hidden = ['id'];


        #relaciones
        #1-1 con estado
    public function estado()
        {

            return $this->belongsTo('App\Estado');
           

        }

    # muchos - muchos con autores

    public function autores()
        {

            return $this->belongsToMany('App\Autor');

        }
    #muchos - muchos con idiomas
    public function idiomas(){


            return $this->belongsToMany('App\Idioma');
    }
    #muchos a muchos con cotegoria
    public function categorias(){

        return $this->belongsToMany('App\Categoria');

    }

    public function editoriales(){

        return $this->belongsToMany('App\Editorial');
    }

    public function detalle_compra(){

        return $this->belongsTo('App\Detalle_compra');
    }

    public function arriendos(){

        return $this->hasMany('App\Arriendo_libro');
    }


     #función que permite crear un buscador usando SCOPE
     public function scopeSearch($query, $name) #segundo parámetro es el que se manda

     {
         return $query->where('titulo','LIKE',"%$name%");
 
     }
         public function scopeSearchTag($query, $name)
     {
 
         return $query->where('titulo','=',$name);
     }

}