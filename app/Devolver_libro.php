<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Devolver_libro extends Model
{
    use notifiable;
    protected $table = "devolver_libro";

    protected $fillable = [
        'id',
        'multa',
        'costo_total',
        'dias_retraso',
        'fecha_devolucion',
        'arriendo_libro_id'
    ];
    #protected $hidden = ['id'];

    public function arriendo()
    {

        return $this->belongsTo('App\Arriendo_libro');
       

    }


}
