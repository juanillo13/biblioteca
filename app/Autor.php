<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    use Notifiable;
    protected $table = "autor";

    protected $fillable = [
        'id',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'libro_id'
    ];

    protected $hidden = ['id'];

    public function libros(){

        return $this->belongsToMany('App\Libro')->withTimestamps();

    }
            #función que permite crear un buscador usando SCOPE
            public function scopeSearch($query, $name) #segundo parámetro es el que se manda

            {
                return $query->where('nombre','LIKE',"%$name%");
        
            }
                public function scopeSearchTag($query, $name)
            {
        
                return $query->where('nombre','=',$name);
            }


}
