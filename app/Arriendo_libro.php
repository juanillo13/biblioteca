<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Arriendo_libro extends Model
{
    use notifiable;
    protected $table = "arriendo_libro";

    protected $fillable = [
        'id',
        'libro_id',
        'cliente_id',
        'costo_arriendo',
        'fecha_arriendo',
        'fecha_devolucion_estimada',
        'trabajador_id'
    ];
    #protected $hidden = ['id'];

    public function libro(){

        return $this->belongsTo('App\Libro','libro_id');
    }

    public function cliente(){

        return $this->belongsTo('App\Cliente', 'cliente_id');
    }

    public function trabajador(){

        return $this->belongsTo('App\Trabajador','trabajador_id');
    }

    public function Devolver(){

        return $this->hasOne('App\Devolver_libro');

    }

}
