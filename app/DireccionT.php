<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class DireccionT extends Model
{
    use notifiable;

    protected $table = "direccionT";

    protected $fillable =[
        'id',
        'direccion',
        'trabajador_id'
    ];

    public function trabajador(){

        return $this->belongsTo('App\Trabajador');
    }


     #función que permite crear un buscador usando SCOPE
     public function scopeSearch($query, $name) #segundo parámetro es el que se manda

     {
         return $query->where('direccion','LIKE',"%$name%");
 
     }
         public function scopeSearchTag($query, $name)
     {
 
         return $query->where('direccion','=',$name);
     }

}
