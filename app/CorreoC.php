<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class CorreoC extends Model
{
    use notifiable;
    protected $table = "correoC";

    protected $fillable = [
        'id',
        'correo',
        'cliente_id'
    ];

    public function cliente(){

        return $this->belongsTo('App\Cliente');
    }

    #función que permite crear un buscador usando SCOPE
    public function scopeSearch($query, $name) #segundo parámetro es el que se manda

    {
        return $query->where('correo','LIKE',"%$name%");

    }
        public function scopeSearchTag($query, $name)
    {

        return $query->where('correo','=',$name);
    }
}
