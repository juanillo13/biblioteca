<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    use notifiable;

    protected $table = "trabajador";

    protected $fillable =[
        'id',
        'rut',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'anio_contrato'
    ];

    #protected $hidden = ['id'];
    #un trabajador puede tener muchos correos, y un correo puede tener solo una persona, es unico
    public function correos(){

        return $this->hasMany('App\CorreoT');
    }

    public function direcciones(){

        return $this->hasMany('App\DireccionT');
    }

    public function telefonos(){

        return $this->hasMany('App\TelefonoT');
    }

    public function boletas(){

        return $this->hasMany('App\Boleta');
    }

    public function arriendos(){

        return $this->hasMany('App\Arriendo_libro');
    }

     #función que permite crear un buscador usando SCOPE
     public function scopeSearch($query, $name) #segundo parámetro es el que se manda

     {
         return $query->where('nombre','LIKE',"%$name%");
 
     }
         public function scopeSearchTag($query, $name)
     {
 
         return $query->where('nombre','=',$name);
     }

}
