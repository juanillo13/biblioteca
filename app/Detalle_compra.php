<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Detalle_compra extends Model
{
    use notifiable;
    protected $table = "detalle_compra";

    protected $fillable = [
        'id',
        'precio_neto',
        'precio_iva',
        'costo_iva',
        'factura_id',
        'libro_id'
    ];
    #protected $hidden = ['id'];

    public function factura(){

        return $this->belongsTo('App\Factura');
    }
    public function libros(){

        return $this->hasMany('App\Libro');
    }
}
