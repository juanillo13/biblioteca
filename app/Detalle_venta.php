<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Detalle_venta extends Model
{
    use notifiable;
    protected $table = "detalle_venta";

    protected $fillable = [
        'id',
        'precio_neto',
        'precio_iva',
        'costo_iva',
        'boleta_id',
        'libro_id'
    ];
    #protected $hidden = ['id'];

    public function boleta(){

        return $this->belongsTo('App\Boleta');
    }
    public function libros(){

        return $this->hasMany('App\Libro');
    }
}
