<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Boleta extends Model
{
    use notifiable;
    protected $table = "boleta";
    #fecha compra esta malo pero se me quedo asi en la bd
    #es fecha_venta 
    protected $fillable =[
        'id',
        'hora_venta',
        'fecha_compra',
        'cliente_id',
        'trabajador_1',
        'metodo_pago_id'
    ];

    #protected $hidden = ['id'];

    public function cliente(){

        return $this->belongsTo('App\Cliente');
    }
    public function trabajador(){

        return $this->belongsTo('App\Trabajador');
    }
    public function metodo_pago(){

        return $this->belongsTo('App\Metodo_pago');

    }
    public function detalles_ventas(){

        return $this->hasMany('App\Detalle_venta');
    }

}
