<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Metodo_pago extends Model
{
    use notifiable;
    protected $table = "metodo_pago";

    protected $fillable =[
        'id',
        'metodo_pago'
    ];

        #protected $hidden = ['id'];

        public function factura(){

            return $this->hasOne('App\Factura');
        }

        public function boleta(){

            return $this->hasOne('App\Boleta');
        }

             #función que permite crear un buscador usando SCOPE
     public function scopeSearch($query, $name) #segundo parámetro es el que se manda

     {
         return $query->where('metodo_pago','LIKE',"%$name%");
 
     }
         public function scopeSearchTag($query, $name)
     {
 
         return $query->where('metodo_pago','=',$name);
     }
}
