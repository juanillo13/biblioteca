<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class TelefonoT extends Model
{
    use notifiable;

    protected $table = "telefonoT";

    protected $fillable =['id','telefono','trabajador_id'];

    public function trabajador(){

        return $this->belongsTo('App\Trabajador');
    }

            #función que permite crear un buscador usando SCOPE
            public function scopeSearch($query, $name) #segundo parámetro es el que se manda

            {
                return $query->where('telefono','LIKE',"%$name%");
        
            }
                public function scopeSearchTag($query, $name)
            {
        
                return $query->where('telefono','=',$name);
            }
}
