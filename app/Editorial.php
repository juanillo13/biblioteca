<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Editorial extends Model
{
    use Notifiable;
    protected $table = "editorial";
    protected $fillable = ['id','nombre_editorial'];
    protected $hidden = ['id'];

    public function libros(){

        return $this->belongsToMany('App\Libro')->withTimestamps();
    }

       #función que permite crear un buscador usando SCOPE
       public function scopeSearch($query, $name) #segundo parámetro es el que se manda

       {
           return $query->where('nombre_editorial','LIKE',"%$name%");
   
       }
           public function scopeSearchTag($query, $name)
       {
   
           return $query->where('nombre_editorial','=',$name);
       }
}
