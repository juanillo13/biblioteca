<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use notifiable;

    protected $table = "cliente";

    protected $fillable = [
        'id',
        'rut',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'fecha_nacimiento'
    ];

    #protected $hidden = ['id'];


    #relaciones 
    #un cliente puede tener muchos correos,telefonos,direcciones
    public function correos(){

        return $this->hasMany('App\CorreoC');
    }

    public function direcciones(){

        return $this->hasMany('App\DireccionC');
    }

    public function telefonos(){

        return $this->hasMany('App\TelefonoC');

    }
    public function boletas(){

        return $this->hasMany('App\Boleta');
    }
    public function arriendos(){

        return $this->hasMany('App\Arriendo_libro');
    }

    #función que permite crear un buscador usando SCOPE
    public function scopeSearch($query, $name) #segundo parámetro es el que se manda

    {
        return $query->where('nombre','LIKE',"%$name%");

    }
        public function scopeSearchTag($query, $name)
    {

        return $query->where('nombre','=',$name);
    }
}
