<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use Notifiable;    
    protected $table = "categoria";
    protected $fillable = ['id','categoria'];
    #protected $hidden = ['id'];


    public function libros(){

        return $this->belongsToMany('App\Libro')->withTimestamps();
    }

           #función que permite crear un buscador usando SCOPE
           public function scopeSearch($query, $name) #segundo parámetro es el que se manda

           {
               return $query->where('categoria','LIKE',"%$name%");
       
           }
               public function scopeSearchTag($query, $name)
           {
       
               return $query->where('categoria','=',$name);
           }
}
