<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { #funcion anonima que devuelve una vista, se deja el nombre de la vista si el .blade.php
    return view('welcome');
}); #ya no se ocupa 
/*
#nota:para devolver una vista que esta dentro de una carpeta se hace .. carpeta.nombredelavista


#ejemplo
#ruta de tip get -> prueba es el nombre y  le estoy pasando un argumento a la ruta
Route::get('prueba/{nombre}', function ($nombre){
    echo "Hola ".$nombre;
});

#ejemplo
#vamos a usasar un prefijo esto quiere decir que la ruta seria ../pruebas/prueba/algo
Route::prefix('pruebas')->group(function () {
    Route::get('prueba/{nombre}', function ($nombre){
        echo "Hola ".$nombre;
    });

    Route::get('prueba2/{nombre}', function ($nombre){
        echo "Hola ".$nombre;
    });

    Route::get('prueba3/{id}','ControladorPrueba@traerLibro')->name('traerLibros');
});
*/

Auth::routes();

Route::get('/administrador', 'HomeController@index')->name('administrador')->middleware('auth');
Route::get('/trabajador', 'HomeController@index2')->name('trabajador')->middleware('auth'); 
Route::get('/','ControladorInicio@Index')->name('visiante.index');

/*
    debemos explicar lo que hace resource
    basicamente nos crea un a serie de rutas del tipo rest full 
    nos crea ControladorUsuarios@index, ..@store , ..@create, ..@destroy , .. @update , ..@show, ..@update , ..@edit
    entonces tenemos que crear estos métodos en el controlador correspondiente :D 
     o hacer make:controller nombrecontrolador --resource
*/

Route::middleware('auth')->group(function () {
    Route::prefix('administrador')->group(function(){

        Route::resource('usuarios','ControladorUsuarios');
        Route::get('usuarios/{id}/destroy', 'ControladorUsuarios@destroy')->name('usuarios.destroy');
        
        Route::resource('estados','ControladorEstados');
        Route::get('estados/{id}/destroy', 'ControladorEstados@destroy')->name('estados.destroy');

        Route::resource('editoriales','ControladorEditoriales');
        Route::get('editoriales/{id}/destroy', 'ControladorEditoriales@destroy')->name('editoriales.destroy');
        
        Route::resource('categorias','ControladorCategorias');
        Route::get('categorias/{id}/destroy', 'ControladorCategorias@destroy')->name('categorias.destroy');
        
        Route::resource('idiomas','ControladorIdiomas');
        Route::get('idiomas/{id}/destroy', 'ControladorIdiomas@destroy')->name('idiomas.destroy');

        Route::resource('autores','ControladorAutores');
        Route::get('autores/{id}/destroy', 'ControladorAutores@destroy')->name('autores.destroy');
        
        Route::resource('libros','ControladorLibros');
        Route::get('libros/{id}/destroy', 'ControladorLibros@destroy')->name('libros.destroy');

        Route::resource('trabajadores','ControladorTrabajadores');
        Route::get('trabajadores/{id}/destroy', 'ControladorTrabajadores@destroy')->name('trabajadores.destroy');

        Route::resource('correost','ControladorCorreoT');
        Route::get('correost/{id}/destroy', 'ControladorCorreoT@destroy')->name('correost.destroy');
        
        Route::resource('telefonost','ControladorTelefonoT');
        Route::get('telefonost/{id}/destroy', 'ControladorTelefonoT@destroy')->name('telefonost.destroy');

        Route::resource('direccionest','ControladorDireccionT');
        Route::get('direccionest/{id}/destroy', 'ControladorDireccionT@destroy')->name('direccionest.destroy');

        Route::resource('clientes','ControladorClientes');
        Route::get('clientes/{id}/destroy', 'ControladorClientes@destroy')->name('clientes.destroy');

        Route::resource('correosc','ControladorCorreoC');
        Route::get('correosc/{id}/destroy', 'ControladorCorreoC@destroy')->name('correosc.destroy');

        Route::resource('telefonosc','ControladorTelefonoC');
        Route::get('telefonosc/{id}/destroy', 'ControladorTelefonoC@destroy')->name('telefonosc.destroy');

        Route::resource('direccionesc','ControladorDireccionC');
        Route::get('direccionesc/{id}/destroy', 'ControladorDireccionC@destroy')->name('direccionesc.destroy');

        Route::resource('pagos','ControladorPagos');
        Route::get('pagos/{id}/destroy', 'ControladorPagos@destroy')->name('pagos.destroy');

        Route::resource('distribuidores','ControladorDistribuidores');
        Route::get('distribuidores/{id}/destroy', 'ControladorDistribuidores@destroy')->name('distribuidores.destroy');
    });

    Route::prefix('trabajador')->group(function(){

        Route::resource('arriendos','ControladorArriendo');
        Route::resource('devoluciones','ControladorDevolucion');
        Route::post('devoluciones/search', 'ControladorDevolucion@search')->name('devoluciones.search');
        Route::get('devoluciones/results/{id}', 'ControladorDevolucion@results')->name('devoluciones.results');

    });
    
});
